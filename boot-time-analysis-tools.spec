Name:               boot-time-analysis-tools
Version:            0.1
Release:            1%{?dist}
Summary:            Collection of tools to analyse boot time.

License:            LGPLv2
URL:                https://gitlab.com/CentOS/automotive/src/boot-time-analysis-tools.git

Source:             boot-time-analysis-tools-4aeed5fb.tar.gz

BuildRequires:      meson

%description
Collection of tools to analyse boot time.


%package systemd
Summary:            Systemd units for the boot time analysis tools.
BuildArch:          noarch
BuildRequires:      systemd-rpm-macros
Requires:           boot-time-analysis-tools

%description systemd
Systemd units for the boot time analysis tools.


%package dracut
Summary:            Dracut modules for the boot time analysis tools.
BuildArch:          noarch
Requires:           boot-time-analysis-tools boot-time-analysis-tools-systemd

%description dracut
Dracut modules for the boot time analysis tools.


%prep
%setup -T -b 0 -q -n boot-time-analysis-tools


%build
%ifarch aarch64
cd cntvct-log
%meson
%meson_build
%endif


%install
%ifarch aarch64
cd cntvct-log
%meson_install
install -DpZm 0644 usr/lib/systemd/system/cntvct@.service %{buildroot}%{_unitdir}/cntvct@.service
install -DpZm 0755 usr/lib/dracut/modules.d/90cntvct/module-setup.sh %{buildroot}%{_prefix}/lib/dracut/modules.d/90cntvct/module-setup.sh
%endif


%files
%ifarch aarch64
%{_bindir}/cntvct
%endif

%files systemd
%ifarch aarch64
%{_unitdir}/cntvct@.service
%endif

%files dracut
%ifarch aarch64
%{_prefix}/lib/dracut/modules.d/90cntvct/module-setup.sh
%endif


%post systemd
%ifarch aarch64
%systemd_post cntvct@.service
%endif

%preun systemd
%ifarch aarch64
%systemd_preun cntvct@.service
%endif

%postun systemd
%ifarch aarch64
%systemd_postun_with_restart cntvct@.service
%endif


%changelog
* Fri Mar 15 2024 Eric Chanudet <echanude@redhat.com> 0.1
- cntvct-log, a simple tool to log the arch counter at various targets. This is
  only provided for aarch64, as it is architecture specific.
